# Hierarchical Data

SQL code to create and populate different designs for hierarchical data. This is used for my Hierarchical Data post: https://www.databasestar.com/hierarchical-data-sql/

This will evolve over time to better structure the code and include examples.